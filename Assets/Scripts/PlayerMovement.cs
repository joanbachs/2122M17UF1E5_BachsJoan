﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rb;

    public Transform playerTransform;
    public float speed = 2f;
    public bool isGrounded = false;
    private bool isFlipped = false;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        Jump();
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            //spriteRenderer.flipX = false;
            if (!isFlipped)
            {
                Flip();
            }

        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            //spriteRenderer.flipX = true;
            if (isFlipped)
            {
                Flip();
            }
        }
        if (isFlipped)
        {
            transform.Translate(-horizontal * Time.deltaTime * speed, 0f, 0f);
        }else if(!isFlipped)
        {
            transform.Translate(horizontal * Time.deltaTime * speed, 0f, 0f);
        }
    }

    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded == true)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 5f), ForceMode2D.Impulse);
        }
    }

    void Flip()
    {
        transform.Rotate(0f, 180f, 0f);
        isFlipped = !isFlipped;

    }
}
