﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class GOscript : MonoBehaviour
{
    public Text score;
    public Text enemies;


    private void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        score.text = "Score: " + GameManager.Instance.Score.ToString("n1");
        enemies.text = "Killed enemies: " + GameManager.Instance.KilledEnemies;
    }

  
}
