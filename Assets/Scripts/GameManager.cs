﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public GameObject Player;
    public ScriptUI scriptUI;

    private float _score;
    private int _killedEnemies;

    public float Score
    {
        get { return _score; }
    }

    public int KilledEnemies
    {
        get { return _killedEnemies; }
    }

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }

    void Start()
    {
        _score = 0;

        Player = GameObject.Find("Player");
        scriptUI = GameObject.Find("Canvas").GetComponent<ScriptUI>();
    }

    void Update()
    {
        
    }

    public void IncreaseScore(float increment)
    {
        _score += increment;
    }
    public void IncreaseEnemiesKilled(int increment)
    {
        _killedEnemies += increment;
        Debug.Log(_killedEnemies.ToString());
    }

    public void GameOver()
    {
        HudManager.Instance.gameUI.SetActive(false);
        HudManager.Instance.gameOverUI.SetActive(true);
    }
}
