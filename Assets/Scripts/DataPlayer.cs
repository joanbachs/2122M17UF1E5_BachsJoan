﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataPlayer : MonoBehaviour
{
    [SerializeField]
    public int lives;
    private int maxLives = 3;

    public Transform spawnPoint;
    public Transform playerPos;
    // Start is called before the first frame update
    void Start()
    {
        lives = 2;
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        spawnPoint = GameObject.FindGameObjectWithTag("Respawn").transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void increaseLives()
    {
        if(lives >= maxLives)
        {

        }
        else
        {
            lives++;
        }
    }
}
