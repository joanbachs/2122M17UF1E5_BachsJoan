﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScriptUI : MonoBehaviour
{
    public Text VidesText;
    public Text ScoreText;

    void Start()
    {

    }

    void Update()
    {
        VidesText.text = "Lifes: " + GameManager.Instance.Player.GetComponent<DataPlayer>().lives;
        ScoreText.text = "Meters: " + GameManager.Instance.Score.ToString("n1");
    }
}
