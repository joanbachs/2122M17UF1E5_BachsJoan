﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPlayer : MonoBehaviour
{
    public DataPlayer dataPlayer;
    private GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Deadzone"))
        {
            Debug.Log("Colisió amb el player");

            //gameObject.SetActive(false);
            Destroy(gameObject);
            GameManager.Instance.GameOver();

        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            dataPlayer.lives--;
            dataPlayer.playerPos.position = new Vector3(dataPlayer.spawnPoint.position.x, dataPlayer.spawnPoint.position.y, 0);

            if (dataPlayer.lives <= 0)
            {
                Destroy(gameObject);
                GameManager.Instance.GameOver();
            }
        }

        if (other.gameObject.CompareTag("Food"))
        {
            dataPlayer.increaseLives();
            Destroy(other);
        }
    }
}
