﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    
    [SerializeField]
    public Transform target;
    //public Vector3 offset;

    private float maxY;
    public GameManager gm;
    private float score;
    private float scoreInitial;

    /*
    void LateUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;
    }
    */
    void Start()
    {
        maxY = target.position.y;
        transform.position = new Vector3(transform.position.x, target.position.y + 2.5f, -10);
        score = 0;
        scoreInitial = maxY;

    }
    void Update()
    {
        if (maxY <= target.position.y)
        {
            transform.position = new Vector3(transform.position.x, target.position.y+2.5f, -10);
            maxY = target.position.y;

            score += (maxY - scoreInitial);
            score /= 100;
            score.ToString("n2");
            gm.IncreaseScore(score);
        }
    }
}
